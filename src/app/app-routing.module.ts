import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";

import { LoginComponent } from "./login/login.component";
import { MovieDetailsComponent } from "./movie-details/movie-details.component";
import { MoviesListComponent } from "./movies-list/movies-list.component";
import { ErrorComponent } from "./error/error.component";

import { AuthGuard } from "./guards/auth.guard";

import { MovieDetailsResolver } from "./resolvers/movie-details.resolver";
import { MoviesResolver } from "./resolvers/movies.resolver";
import { AlreadyLoggedinGuard } from "./guards/already-loggedin.guard";

const appRoutes: Route[] = [
  {path: '', redirectTo: '/movies', pathMatch: 'full'},
  {path: 'movies', canActivate: [AuthGuard], component: MoviesListComponent, resolve: {movies: MoviesResolver}},
  {path: 'movies/:id', canActivate: [AuthGuard], component: MovieDetailsComponent, resolve: {movieDetails: MovieDetailsResolver}},
  {path: 'login', canActivate: [AlreadyLoggedinGuard] ,component: LoginComponent},
  {path: 'not-found', canActivate: [AuthGuard], component: ErrorComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}

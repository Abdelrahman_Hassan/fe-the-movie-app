import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isIncorrectCredentials: boolean = false;
  isInvalidEmail: boolean = false;
  invalidEmailMessage: string = 'This Email is invalid!';
  incorrectCredentialsMessage: string = 'Incorrect Credentials';

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    if(this.authService.autoLogin()) {
      this.router.navigate(['/']);
    }
  }

  onBlur(form: NgForm) {
    if(form.form.get('email')?.invalid)
      this.isInvalidEmail = true;
    else
      this.isInvalidEmail = false;
  }

  onLogin(form: NgForm) {
    if(this.authService.login(form.value.email, form.value.password)){
      this.isIncorrectCredentials = false;
      this.router.navigate(['/']);
    } else {
      this.isIncorrectCredentials = true;
    }
  }
}

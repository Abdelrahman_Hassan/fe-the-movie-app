import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { User } from "../models/user.model";

@Injectable({providedIn: 'root'})
export class AuthService {
  users: User[] = [
    {
      email: 'admin@test.com',
      password: '123456',
      name: 'Admin'
    },
    {
      email: 'abdelrahman@test.com',
      password: '123456',
      name: 'Abdelrahman'
    },
  ];
  loggedInUser!: User;
  isLoggedIn: boolean = false;
  isLoggedInSubject = new Subject<boolean>();

  autoLogin() {
    let loggedinUserString = localStorage.getItem('user');

    if(!loggedinUserString)
      return false;

    this.loggedInUser = JSON.parse(loggedinUserString);
    this.isLoggedIn = true;
    this.isLoggedInSubject.next(this.isLoggedIn);

    return true;
  }

  login(email: string, password: string) {
    for(let user of this.users) {
      if(user.email == email && user.password == password){
        this.isLoggedIn = true;
        this.loggedInUser = user;
        this.isLoggedInSubject.next(this.isLoggedIn);
        localStorage.setItem('user', JSON.stringify(user));
      }
    }


    return this.isLoggedIn;
  }

  logout(){
    this.isLoggedIn = false;
    localStorage.clear();
    this.isLoggedInSubject.next(this.isLoggedIn);
  }


}

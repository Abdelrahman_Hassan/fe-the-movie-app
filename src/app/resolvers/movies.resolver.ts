import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { MoviesService } from "../services/movies.service";

@Injectable({providedIn: 'root'})
export class MoviesResolver implements Resolve<any>{
  constructor(private moviesService: MoviesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.moviesService.getMovies();
  }
}

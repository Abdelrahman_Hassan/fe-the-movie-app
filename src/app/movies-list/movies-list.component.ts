import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Movie } from '../models/movie.model';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  movies!: Movie[];
  subscription!: Subscription;
  constructor(private moviesService: MoviesService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {

    // this.route.data.subscribe(
    //   (data) => {
    //     this.movies = data['movies'].results;
    //   }
    // );

    this.moviesService.getMovies().subscribe(
      (data) => {
            this.movies = data.results;
          }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  name: string = "Name";

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.name = this.authService.loggedInUser?.name;
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieDetails } from '../models/movie-details.model';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  movieDetails!: MovieDetails;
  hours!: number;
  minutes!: number;
  movieDuration!: string;

  constructor(private moviesService: MoviesService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe(
      (data) => {
        this.movieDetails = data['movieDetails'];
        this.hours = Math.floor(this.movieDetails.runtime/60);
        this.minutes = this.movieDetails.runtime - this.hours * 60;
        this.movieDuration = (this.hours >= 10 ? this.hours : '0' + this.hours) + ':' + (this.minutes >= 10 ? this.minutes : '0' + this.minutes);
      }
    );
  }

}

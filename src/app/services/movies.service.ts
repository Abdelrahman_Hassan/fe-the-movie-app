import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { MovieDetails } from "../models/movie-details.model";
import { Movies } from "../models/movies.model";


@Injectable({providedIn: 'root'})
export class MoviesService {
  // private movies!: Movie[];
  private apiKey = '319b589d4bf702639d689f6d5588ff8b';
  private baseUrl = 'https://api.themoviedb.org/3/';
  private topRatedUrl = this.baseUrl + 'movie/top_rated?api_key=' + this.apiKey + '&language=en-US';
  imagesBasePath = 'https://image.tmdb.org/t/p/original';


  constructor(private http: HttpClient,
              private router: Router) {}

  getMovies() {
    return this.http.get<Movies>(this.topRatedUrl);
  }

  getMovieDetails(movieId: number) {
    return this.http.
    get<MovieDetails>(this.baseUrl + 'movie/' + movieId + '?api_key=' + this.apiKey + '&language=en-US');
  }
}

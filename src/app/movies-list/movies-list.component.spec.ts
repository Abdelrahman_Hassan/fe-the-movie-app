import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { MoviesService } from '../services/movies.service';
import { of } from 'rxjs';

import { MoviesListComponent } from './movies-list.component';
import { Movies } from '../models/movies.model';

describe('MoviesListComponent', () => {
  let component: MoviesListComponent;
  let fixture: ComponentFixture<MoviesListComponent>;
  let service: MoviesService;
  let dummyData = {
    "page": 1,
    "results": [
      {
        "original_title": "Cosas imposibles",
        "poster_path": "https://image.tmdb.org/t/p/original/eaf7GQj0ieOwm08rrvjJQNbN0kN.jpg",
        "vote_average": 9,
      },
      {
        "original_title": "The Shawshank Redemption",
        "poster_path": "https://image.tmdb.org/t/p/original/q6y0Go1tsGEsmtFryDOJo3dEmqu.jpg",
        "vote_average": 8.7,
      }
    ],
    "total_pages": 493,
    "total_results": 9855
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        AppRoutingModule,
        FormsModule
      ],
      declarations: [ MoviesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesListComponent);
    service = fixture.debugElement.injector.get(MoviesService);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(service, 'getMovies').and.returnValue(of<any>(dummyData));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load movies in DOM', fakeAsync(() => {
    const fixture = TestBed.createComponent(MoviesListComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    tick();

    const rows = compiled.querySelector('tbody').querySelectorAll('tr');
    let loadedMovies: {"original_title": string, "poster_path": string, "vote_average": number}[] = [];
    for(let tr of rows){
      loadedMovies.push(
        {
          "original_title": tr.querySelectorAll('td')[0].textContent,
          "poster_path": tr.querySelectorAll('td')[1].querySelector('img').src.slice(35),
          "vote_average": +tr.querySelectorAll('td')[2].textContent
        }
      );
    }
    expect(loadedMovies).toEqual(dummyData.results);
  }));
});

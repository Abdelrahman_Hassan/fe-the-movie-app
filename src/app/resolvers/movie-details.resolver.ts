import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { of } from "rxjs";
import { MoviesService } from "../services/movies.service";
import { catchError } from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class MovieDetailsResolver implements Resolve<any>{
  constructor(private moviesService: MoviesService,
              private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.moviesService.getMovieDetails(route.params['id']).pipe(catchError(
      (error) => {
        this.router.navigate(['/not-found']);
        return of({error: error});
      }
    ));
  }

}
